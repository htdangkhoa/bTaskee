var gulp = require("gulp"),
    browserSync = require("browser-sync"),
    sass = require("gulp-sass");

gulp.task("sass", () => {
    return gulp.src("./src/styles/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("./src/styles"))
    .pipe(browserSync.stream());
});

gulp.task("serve", ["sass"], function() {
    browserSync.init({
        server: "./src"
    });

    gulp.watch("./src/styles/*.scss", ["sass"]);
    gulp.watch("./src/*.html").on("change", browserSync.reload);
    gulp.watch("./src/js/*.js").on("change", browserSync.reload);
});

gulp.task("default", ["serve"]);